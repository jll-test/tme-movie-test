import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useNavigate } from "react-router";

// Data grid to show list of movie search results, but use whatever you like here.
import { DataGrid, GridColDef } from "@mui/x-data-grid";

// Text field to allow user to enter search text, but use whatever you like here. 
import TextField from '@mui/material/TextField';

const MovieSearchPage = (props) => {
  // Use hooks to retrieve list of movies based on user input of search text.  After user enters 4th character in search box,
  // retrieval should happen automatically.

  // Render search results here.  Refer to README for specifications.
  return (
    <>
      <h5>Search Box here</h5>
      <h3>Search Results here</h3>
    </>
  )
};

export default MovieSearchPage;
